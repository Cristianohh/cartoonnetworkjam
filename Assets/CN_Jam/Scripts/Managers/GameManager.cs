﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Assets.Managers;
using System.Collections;
using UnityEngine.SceneManagement;
using Assets.Data;

namespace Assets.Managers {
	public class GameManager : MonoBehaviour {

		public Image[] CoffeeCups = null; 

		public Text CountDownTimerText = null;
		public Text ScoreText = null; 
		public Text MultiplyerText = null;

		public GameObject YouLoseBanner = null;

		//[HideInInspector]
		public static GameManager Singleton = null;

		// timer stuff
		float StartingCountdownTime = 10.0f;
		float MinimumCountdownTime = 5.0f;
		float CountdownTimeRemaining = 0.0f;
		float CountdownTime = 0.0f;
		float NumSecondsToDecrementPerCorrect = 0.5f;

		public Slider TimeMeter = null;
		public Image TimeMeterFill = null;
		public Color ColdColor;
		public Color BurnColor;

		public STATE CurrentState = STATE.HECKLE;

		public GameObject IntroSplash;

		JokeManager.Joke CurrentJoke = null;

		public Button IntroButton = null;
		public Button TutorialButton = null;

		public SpriteRenderer LosingSign;
		public SpriteRenderer WinningSign;

		// multiplyer stuff
		const string MultiplyerLabel = "x";
		public int CurrentMultiplyer = 1;
		int MaxMultiplyer = 4;

		bool PlayerIsAlive = true;

		public bool Debugging = true;

        public bool IsBoiling = false;

		// Use this for initialization
		void Start () {
			if(Singleton == null)
			{
				Singleton = this;
				Singleton.InitManager();
			}
		}

		void InitManager()
		{
			Assert.AreNotEqual(null, CountDownTimerText);
			Assert.AreNotEqual(null, ScoreText);
			Assert.AreNotEqual(null, MultiplyerText);
			Assert.AreEqual(3, CoffeeCups.Length);
			CountdownTime = StartingCountdownTime;
			CountdownTimeRemaining = CountdownTime;
			TimeMeter.maxValue = CountdownTime;

			ColdColor = PlayerController.Singleton.ColdColor;
			BurnColor = PlayerController.Singleton.BurnColor;
			MultiplyerText.text = MultiplyerLabel + CurrentMultiplyer;


			if(Debugging){
				CurrentState = STATE.INITIALIZE_GAME;
			}
			else
			{
				DisableTimerVisuals();
				IntroButton.onClick.AddListener( () => ClickedStart());
				TutorialButton.onClick.AddListener( () => ClickedDoneTutorial());
				TimeMeter.gameObject.SetActive(false);
				IntroButton.gameObject.SetActive(true);
			}

			AudioManager.Singleton.SetAudioSourceVol(AudioManager.AUDIO_SOURCE_ID.MUSIC_SOURCE, 1.0f);
			AudioManager.Singleton.PlayMusicSound(AudioManager.MUSIC_ID.INTRO_LOOP);
			SetCupGraphic();

			StartCoroutine("Co_Update");


		}

		void SetCupGraphic()
		{
			for(int i = 0; i < CoffeeCups.Length; i++)
			{
				if(i < (CurrentMultiplyer - 1))
				{
					CoffeeCups[i].enabled = true;
				}
				else
				{
					CoffeeCups[i].enabled = false;
				}
			}
		}

		public void ClickedStart()
		{
			AudioManager.Singleton.PlaySFXSound(AudioManager.SFX_ID.MENU_CLICK);
			IntroButton.gameObject.SetActive(false);
			TutorialButton.gameObject.SetActive(true);
		}

		public void ClickedDoneTutorial()
		{
			AudioManager.Singleton.PlaySFXSound(AudioManager.SFX_ID.MENU_CLICK);
			TutorialButton.gameObject.SetActive(false);
			CurrentState = STATE.INITIALIZE_GAME;
		}

		public void DisableTimerVisuals()
		{
			if(TimeMeter.gameObject.activeInHierarchy == true && TimeMeter.gameObject != null)
				TimeMeter.gameObject.SetActive(false);
		}
		public void EnableTimerVisuals()
		{
			if(TimeMeter.gameObject.activeInHierarchy == false && TimeMeter.gameObject != null)
				TimeMeter.gameObject.SetActive(true);
		}

		void UpdateTimerColor()
		{
			Color sliderColor;
			float nrmalVal = TimeMeter.value / (TimeMeter.maxValue - TimeMeter.minValue);

			sliderColor = Color.Lerp(BurnColor, ColdColor, nrmalVal);
			TimeMeterFill.color = sliderColor;
		}

		/*
		void AddPoints(int numPoints)
		{
			CurrentPoints += NumPointsPerCorrectHit * CurrentMultiplyer;
			ScoreText.text = ScoreLabel + " " + CurrentPoints;
		}
*/
		public void Lose()
		{
            PlayerIsAlive = false;
            AudioManager.Singleton.SetAudioSourceVol(AudioManager.AUDIO_SOURCE_ID.MUSIC_SOURCE, 1.0f);
            CurrentState = STATE.LIMBO;
			//YouLoseBanner.SetActive(true);
			AudioManager.Singleton.PlayMusicSound(AudioManager.MUSIC_ID.LOSE_MUSIC);
			JokeManager.Singleton.DisableUI();
			LosingSign.enabled = true;
			PlayerIsAlive = false;

			
			StartCoroutine("ResetGame");
			// turn off Joe Cuppa?
		}

		IEnumerator ResetGame()
		{
			// allow music to finish
			AudioManager.Singleton.SwitchLoopingForSource(AudioManager.AUDIO_SOURCE_ID.MUSIC_SOURCE, false);
			while(AudioManager.Singleton.IsClipPlayingInSource(AudioManager.AUDIO_SOURCE_ID.MUSIC_SOURCE))
				yield return null;
			SceneManager.LoadScene("__MainScene");
		}

		void Win()
		{
            AudioManager.Singleton.SetAudioSourceVol(AudioManager.AUDIO_SOURCE_ID.MUSIC_SOURCE, 1.0f);
            AudioManager.Singleton.PlayCrowdSound(AudioManager.SFX_ID.CROWD_CHEER);
			AudioManager.Singleton.PlayMusicSound(AudioManager.MUSIC_ID.WIN_MUSIC);
			JokeManager.Singleton.DisableUI();
			CurrentState = STATE.LIMBO;
			WinningSign.enabled = true;

			StartCoroutine("ResetGame");
		}

		void UpdateTiming()
		{
			CountdownTimeRemaining -= Time.deltaTime;
			if(CountdownTimeRemaining <= 0.0f && PlayerIsAlive != false)
			{
				Lose();
			}
			TimeMeter.value = CountdownTimeRemaining;
		}

		IEnumerator Co_Update()
		{
			while(PlayerIsAlive)
			{
				if(CurrentState == STATE.INTRO)
				{
				}
				else if(CurrentState == STATE.INITIALIZE_GAME)
				{
                    AudioManager.Singleton.SetAudioSourceVol(AudioManager.AUDIO_SOURCE_ID.MUSIC_SOURCE, 0.3f);
                    AudioManager.Singleton.PlayMusicSound(AudioManager.MUSIC_ID.MAIN_LOOP);
					CurrentState = STATE.HECKLE;
				}
				else if(CurrentState == STATE.HECKLE)
				{


                    CurrentJoke = JokeManager.Singleton.GetRandomJoke();


                    if(PlayerIsAlive)
                    {
                        JokeManager.Singleton.DisableUI();
                        AnimationController.Singleton.SwitchCuppaPoseForActor(CurrentJoke.Actor);

                        JokeManager.Singleton.OpenSpeachBubble(CurrentJoke.Actor, CurrentJoke.RobotHeckle);
                        AudioManager.Singleton.PlayNarrationClip(CurrentJoke.Actor, CurrentJoke.index, JOKE_ID.HECKLE);

                        while (AudioManager.Singleton.IsClipPlayingInSource(AudioManager.AUDIO_SOURCE_ID.VOICE_ACTING))
                            yield return null;
                        AnimationController.Singleton.SwitchCuppaPoseToPoint();
                        CurrentState = STATE.SETUP_READ;
                    }
                }
				else if(CurrentState == STATE.SETUP_READ)
				{
					AudioManager.Singleton.PlayNarrationClip(CurrentJoke.Actor, CurrentJoke.index, JOKE_ID.SETUP);

					
					// Show Selection UI
                    if(CurrentJoke.Actor != ACTOR.DOG)
                    {

                        JokeManager.Singleton.SetUIWithJoke(CurrentJoke);
                        JokeManager.Singleton.ShowUI();
                    }
                    if (MinimumCountdownTime < CountdownTime)
					{
						CountdownTime -= NumSecondsToDecrementPerCorrect;
					}

					CountdownTimeRemaining = CountdownTime;
					TimeMeter.maxValue = CountdownTime;
					UpdateTiming();
					UpdateTimerColor();

					CurrentState = STATE.USER_SELECTING_OPTION;
				}
				else if(CurrentState == STATE.USER_SELECTING_OPTION)
				{
					while(CurrentState == STATE.USER_SELECTING_OPTION)
					{
						UpdateTiming();
						UpdateTimerColor();
						yield return null;
					}
				}
				else if(CurrentState == STATE.CORRECT_RESPONSE)
				{
					JokeManager.Singleton.DisableUI();
					JokeManager.Singleton.CloseSpeachBubble();
					AudioManager.Singleton.PlayNarrationClip(CurrentJoke.Actor, CurrentJoke.index, JOKE_ID.JOKE);
					while(AudioManager.Singleton.IsClipPlayingInSource(AudioManager.AUDIO_SOURCE_ID.VOICE_ACTING))
						yield return null;
					ChooseRightJoke();
					StartCoroutine("Co_BurnChar", CurrentJoke.Actor);
					while(AudioManager.Singleton.IsClipPlayingInSource(AudioManager.AUDIO_SOURCE_ID.RIFF_SOURCE))
						yield return null;
					CurrentJoke = null;

					// check if win
					if(PlayerController.Singleton.DidWin())
					{
						Win();
					}
					else
					{
						CurrentState = STATE.HECKLE;
					}
				}
				else if (CurrentState == STATE.LIMBO)
				{

				}

				yield return null;
			}
		}

		IEnumerator Co_BurnChar(ACTOR actor)
		{
			AnimationController.Singleton.BurnActor(actor);
			yield return new WaitForSeconds(3.0f);
			AnimationController.Singleton.UnBurnActor(actor);
		}

        IEnumerator Co_PlayBoilingSound()
        {
            AudioManager.Singleton.SwitchLoopingForSource(AudioManager.AUDIO_SOURCE_ID.BOILING_SOURCE, false);
            AudioManager.Singleton.PlayBoilingSFXSound(AudioManager.SFX_ID.TEA_KETTLE);
            while (AudioManager.Singleton.IsClipPlayingInSource(AudioManager.AUDIO_SOURCE_ID.BOILING_SOURCE))
                yield return null;
            AudioManager.Singleton.SwitchLoopingForSource(AudioManager.AUDIO_SOURCE_ID.BOILING_SOURCE, true);
            AudioManager.Singleton.PlayBoilingSFXSound(AudioManager.SFX_ID.BOILING_SOUND);
        }

        private void AddBoiling()
        {
            IsBoiling = true;

            // cuppa screen fade in
            AnimationController.Singleton.SetTrigger(ACTOR.JOE_CUPPA_BOILING_SPRITE, Data.StringHashes.CuppaBoilingHash);
            // boiling sprites
            AnimationController.Singleton.UpdateBurnVisualForCurrent();
            StartCoroutine("Co_PlayBoilingSound");
            // steam
        }

        private void RemoveBoiling()
        {
            IsBoiling = false;

            // stop looping boiling sound
            AudioManager.Singleton.StopAudioForSource(AudioManager.AUDIO_SOURCE_ID.BOILING_SOURCE);
            AnimationController.Singleton.UpdateBurnVisualForCurrent();
        }

		public void ChooseRightJoke()
		{
			PlayerController.Singleton.AddBurn();

			CurrentMultiplyer++;
			CurrentMultiplyer = Mathf.Min(CurrentMultiplyer, MaxMultiplyer);

            if(CurrentMultiplyer == MaxMultiplyer && !IsBoiling)
            {
                // activate boiling!
                AddBoiling();
            }

			SetCupGraphic();
			AudioManager.Singleton.PlayRandomPositiveRiff();
			BadassTextManager.Singleton.SpawnText(BadassTextManager.Singleton.GetRandomTextelement());
			AudioManager.Singleton.PlayCrowdSound(AudioManager.SFX_ID.CROWD_LAUGH);
		}

		public void ChooseWrongJoke()
		{
			CurrentMultiplyer = 1;
			SetCupGraphic();
			PlayerController.Singleton.RemoveBurn();
			AudioManager.Singleton.PlayRandomNegativeRiff();
			AudioManager.Singleton.PlayCrowdSound(AudioManager.SFX_ID.CROWD_BOO);
            RemoveBoiling();
        }
	}
}
