﻿//#define DEBUGGING
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using System.Collections;
using Assets.Managers;
using Assets.Data;

namespace Assets.Managers {
	public class PlayerController : MonoBehaviour {

		public static PlayerController Singleton = null;

		public Slider BurnMeter = null;
		public Image BurnMeterFill = null;

		float CurrentBurn = 0.0f;
		float BurnDelta = 3.0f;
		float SliderSpeed = 6f;

		const float ExperienceSnapBias = 0.2f;

		public Color ColdColor;
		public Color MidColor;
		public Color BurnColor;

#if DEBUGGING

		public Button AddBurnButton = null;
		public Button RemoveBurnButton = null;
		void InitDebugging() {
			Assert.AreNotEqual(null, AddBurnButton);
			Assert.AreNotEqual(null, RemoveBurnButton);
			AddBurnButton.onClick.AddListener( () => AddBurn());
			RemoveBurnButton.onClick.AddListener( () => RemoveBurn());
		}
#else
		void InitDebugging() {}
#endif

		// Use this for initialization
		void Start () {

			if(Singleton == null)
			{
				Singleton = this;
				Singleton.InitPlayerController();
				Assert.AreNotEqual(null, BurnMeter);
				Assert.AreNotEqual(null, Singleton);
				Singleton.InitDebugging();
				Assert.AreNotEqual(null, BurnMeterFill);

			}
		}

		void InitPlayerController()
		{
			//BurnMeter.value = (BurnMeter.maxValue - BurnMeter.minValue) * 0.2f;
		}

		// Update is called once per frame
		void Update () {
			UpdateSliderVal();
		}



		void UpdateSliderColor()
		{
			Color sliderColor;
			float nrmalVal = BurnMeter.value / (BurnMeter.maxValue - BurnMeter.minValue);
#if THREE_VAL_LERP
			if(nrmalVal < 0.5f)
			{
				sliderColor = Color.Lerp(ColdColor, MidColor, nrmalVal);
			}
			else
			{
				sliderColor = Color.Lerp(MidColor, BurnColor, nrmalVal);
			}
#else
			sliderColor = Color.Lerp(ColdColor, BurnColor, nrmalVal);
#endif
			BurnMeterFill.color = sliderColor;
		}

		public void AddBurn()
		{
			CurrentBurn += BurnDelta * GameManager.Singleton.CurrentMultiplyer;
			CurrentBurn = Mathf.Min(BurnMeter.maxValue, CurrentBurn);
			AnimationController.Singleton.SetTrigger(ACTOR.JOE_CUPPA_HAPPY_DECAL, Data.StringHashes.CuppaHappyHash);
			UpdateSliderColor();
		}

		public void RemoveBurn()
		{
			CurrentBurn -= BurnDelta;
			CurrentBurn = Mathf.Max(BurnMeter.minValue, CurrentBurn);
			AnimationController.Singleton.SetTrigger(ACTOR.JOE_CUPPA_SAD_DECAL, Data.StringHashes.CuppaPainHash);
			UpdateSliderColor();
		}

		void UpdateSliderVal()
		{
			float currentSliderValue = Mathf.Lerp(BurnMeter.value, CurrentBurn, SliderSpeed * Time.deltaTime);
			//currentSliderValue = (currentSliderValue + ExperienceSnapBias) >= CurrentBurn ? CurrentBurn : currentSliderValue;
			if (currentSliderValue >= BurnMeter.maxValue)
			{
				//currentSliderValue %= BurnMeter.maxValue;
				//CurrentBurn %= BurnMeter.maxValue;
				//CurrentLevel++;
			}

			// If value has changed, update XP val
			if(currentSliderValue != BurnMeter.value)
			{
				BurnMeter.value = currentSliderValue;
				//UpdateExperienceDisplayString();
			}
		}

		public float GetBurnVal()
		{
			return BurnMeter.value;
		}

		public bool DidWin()
		{
			if(BurnMeter.value >= (BurnMeter.maxValue - 3.0f))
			{
				return true;
			}
			return false;
		}
	}
}

