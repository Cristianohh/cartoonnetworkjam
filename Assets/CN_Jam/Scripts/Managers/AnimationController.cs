﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Data;

namespace Assets.Managers {
	public class AnimationController : MonoBehaviour {

		public static AnimationController Singleton = null;

		[System.Serializable]
		public class BurnAnimationInfo
		{
			public SpriteRenderer NonBurnedSprite;
			public SpriteRenderer BurnedSprite;
			public SpriteRenderer[] AdditionalSpritesToDisable;
			public ACTOR ActorID;
		};

		public enum CUPPA_POSE
		{
			FORWARD_STANCE_ARM_OUT,
			FORWARD_STANCE_ARM_DOWN,
			LEFT_CUPPA_ARM_DOWN,
			LEFT_CUPPA_ARM_UP,
			RIGHT_CUPPA_ARM_DOWN,
			RIGHT_CUPPA_ARM_UP,
			MAX_POSES
		}

        [System.Serializable]
        public class CuppaSpritesAnim
        {
            public GameObject RootGameObj;
            public SpriteRenderer NonAnimatedSprite;
            public SpriteRenderer OptionalArm = null;
            public SpriteRenderer AnimatedSprite = null;
            public Animator BoilingAnimator = null;
        };

        [System.Serializable]
        public class CuppaSpritesNew
        {
            public CuppaSpritesAnim ForwardArmUp;
            public CuppaSpritesAnim ForwardArmDown;
            public CuppaSpritesAnim LeftArmUp;
            public CuppaSpritesAnim LeftArmDown;
            public CuppaSpritesAnim RightArmUp;
            public CuppaSpritesAnim RightArmDown;

            public SpriteRenderer SteamSpriteRenderer = null;
            public CUPPA_POSE CurrentPose = CUPPA_POSE.FORWARD_STANCE_ARM_DOWN;
        };

        [System.Serializable]
		public class CuppaSprites
		{
			public GameObject ForwardStanceArmOutRootObj;
			public SpriteRenderer[] ForwardStanceArmOut;
            public Animator BoilingAnimForward = null;
            
            public GameObject ForwardStanceArmDownRootObj;
            public SpriteRenderer[] ForwardStanceArmDown;
            public Animator BoilingAnimForwardDown = null;

            public GameObject LeftCuppaArmDownRootObj;
            public SpriteRenderer[] LeftCuppaArmDown;
            public Animator BoilingAnimLeftDown = null;

            public GameObject LeftCuppaArmUpRootObj;
			public SpriteRenderer[] LeftCuppaArmUp;
            public Animator BoilingAnimLeft = null;

            public GameObject RightCuppaArmDownRootObj;
			public SpriteRenderer[] RightCuppaArmDown;
            public Animator BoilingAnimRightDown = null;

            public GameObject RightCuppaArmUpRootObj;
			public SpriteRenderer[] RightCuppaArmUp;
            public Animator BoilingAnimRight = null;

            public CUPPA_POSE CurrentPose = CUPPA_POSE.FORWARD_STANCE_ARM_DOWN;
		};

        [System.Serializable]
		public class FireInfo
		{
			public SpriteRenderer FireRenderer;
			public ACTOR Actor;
		};

		public FireInfo[] FireStruct;

		public CuppaSprites cuppaPoses;
        public CuppaSpritesNew CuppaPosesNew;

        Dictionary<CUPPA_POSE, SpriteRenderer[]> poseToSpritesDict = new Dictionary<CUPPA_POSE, SpriteRenderer[]>();
		Dictionary<CUPPA_POSE, GameObject> poseToGameObjDict = new Dictionary<CUPPA_POSE, GameObject>();
		Dictionary<ACTOR, CUPPA_POSE> ActorToCuppaGameObj = new Dictionary<ACTOR, CUPPA_POSE>();
        Dictionary<CUPPA_POSE, CuppaSpritesAnim> PosesToSpritesAnimInfo = new Dictionary<CUPPA_POSE, CuppaSpritesAnim>();

        public BurnAnimationInfo[] BurnInfo;

		public Animator CuppaPainAnimator = null;
        public Animator CuppaHappyAnimator = null;
        public Animator CorgiBlinkAnimator = null;
        public Animator CuppaBoilingAnimator = null;

        public Animator JoeCuppaAnimator = null;

        Dictionary<ACTOR, Animator> animDict = new Dictionary<ACTOR, Animator>();

        [Range(0, 1)]
        public float AnimationSpeedLevelOne;
        [Range(0, 1)]
        public float AnimationSpeedLevelTwo;
        [Range(0, 1)]
        public float AnimationSpeedLevelThree;

        CuppaSpritesAnim CurrentAnimInfo = null;

        // Use this for initialization
        void Start () {
			if(Singleton == null)
			{
				Singleton = this;
				Singleton.InitAnimationController();
			}
		}

		void DisableSpritesForPose(SpriteRenderer[] renderer)
		{
			foreach(SpriteRenderer rend in renderer)
			{
				rend.enabled = false;
			}
		}

		void EnableSpritesForPose(SpriteRenderer[] renderer)
		{
			foreach(SpriteRenderer rend in renderer)
			{
				rend.enabled = true;
			}
		}

		public void ChooseRandomCuppaPose()
		{
			CUPPA_POSE chosenPose = CUPPA_POSE.FORWARD_STANCE_ARM_OUT;

			if(chosenPose == CUPPA_POSE.FORWARD_STANCE_ARM_DOWN)
			{

			}
		}

		CUPPA_POSE GetCurrentCuppaPose()
		{
			if(cuppaPoses.ForwardStanceArmOutRootObj.activeInHierarchy)
				return CUPPA_POSE.FORWARD_STANCE_ARM_OUT;
			else if(cuppaPoses.ForwardStanceArmDownRootObj.activeInHierarchy)
				return CUPPA_POSE.FORWARD_STANCE_ARM_DOWN;
			else if(cuppaPoses.LeftCuppaArmUpRootObj.activeInHierarchy)
				return CUPPA_POSE.LEFT_CUPPA_ARM_UP;
			else
				return CUPPA_POSE.LEFT_CUPPA_ARM_DOWN;
		}

		void InitAnimationController()
		{
			animDict[ACTOR.JOE_CUPPA_HAPPY_DECAL] = CuppaHappyAnimator;
            animDict[ACTOR.JOE_CUPPA_SAD_DECAL] = CuppaPainAnimator;
            animDict[ACTOR.CORGI_BLINK] = CorgiBlinkAnimator;
            animDict[ACTOR.JOE_CUPPA] = JoeCuppaAnimator;
            animDict[ACTOR.JOE_CUPPA_BOILING_SPRITE] = CuppaBoilingAnimator;



            PosesToSpritesAnimInfo[CUPPA_POSE.FORWARD_STANCE_ARM_OUT] = CuppaPosesNew.ForwardArmUp;
            PosesToSpritesAnimInfo[CUPPA_POSE.FORWARD_STANCE_ARM_DOWN] = CuppaPosesNew.ForwardArmDown;
            PosesToSpritesAnimInfo[CUPPA_POSE.LEFT_CUPPA_ARM_DOWN] = CuppaPosesNew.LeftArmDown;
            PosesToSpritesAnimInfo[CUPPA_POSE.LEFT_CUPPA_ARM_UP] = CuppaPosesNew.LeftArmUp;
            PosesToSpritesAnimInfo[CUPPA_POSE.RIGHT_CUPPA_ARM_DOWN] = CuppaPosesNew.RightArmDown;
            PosesToSpritesAnimInfo[CUPPA_POSE.RIGHT_CUPPA_ARM_UP] = CuppaPosesNew.RightArmUp;

            poseToSpritesDict[CUPPA_POSE.FORWARD_STANCE_ARM_OUT] = cuppaPoses.ForwardStanceArmOut;
            poseToSpritesDict[CUPPA_POSE.FORWARD_STANCE_ARM_DOWN] = cuppaPoses.ForwardStanceArmDown;
            poseToSpritesDict[CUPPA_POSE.LEFT_CUPPA_ARM_DOWN] = cuppaPoses.LeftCuppaArmDown;
            poseToSpritesDict[CUPPA_POSE.LEFT_CUPPA_ARM_UP] = cuppaPoses.LeftCuppaArmUp;
            poseToSpritesDict[CUPPA_POSE.RIGHT_CUPPA_ARM_DOWN] = cuppaPoses.RightCuppaArmDown;
            poseToSpritesDict[CUPPA_POSE.RIGHT_CUPPA_ARM_UP] = cuppaPoses.RightCuppaArmUp;

            poseToGameObjDict[CUPPA_POSE.FORWARD_STANCE_ARM_OUT] = cuppaPoses.ForwardStanceArmOutRootObj;
            poseToGameObjDict[CUPPA_POSE.FORWARD_STANCE_ARM_DOWN] = cuppaPoses.ForwardStanceArmDownRootObj;
            poseToGameObjDict[CUPPA_POSE.LEFT_CUPPA_ARM_DOWN] = cuppaPoses.LeftCuppaArmDownRootObj;
            poseToGameObjDict[CUPPA_POSE.LEFT_CUPPA_ARM_UP] = cuppaPoses.LeftCuppaArmUpRootObj;
            poseToGameObjDict[CUPPA_POSE.RIGHT_CUPPA_ARM_DOWN] = cuppaPoses.RightCuppaArmDownRootObj;
            poseToGameObjDict[CUPPA_POSE.RIGHT_CUPPA_ARM_UP] = cuppaPoses.RightCuppaArmUpRootObj;

            ActorToCuppaGameObj[ACTOR.ASTRONAUT] = CUPPA_POSE.RIGHT_CUPPA_ARM_DOWN;
			ActorToCuppaGameObj[ACTOR.BALLERINA] = CUPPA_POSE.LEFT_CUPPA_ARM_DOWN;
			ActorToCuppaGameObj[ACTOR.DINOSAUR] = CUPPA_POSE.RIGHT_CUPPA_ARM_DOWN;
			ActorToCuppaGameObj[ACTOR.DOG] = CUPPA_POSE.LEFT_CUPPA_ARM_DOWN;
			ActorToCuppaGameObj[ACTOR.FARMER] = CUPPA_POSE.FORWARD_STANCE_ARM_DOWN;
			ActorToCuppaGameObj[ACTOR.FISHERMAN] = CUPPA_POSE.FORWARD_STANCE_ARM_DOWN;
			ActorToCuppaGameObj[ACTOR.GHOST] = CUPPA_POSE.LEFT_CUPPA_ARM_DOWN;
			ActorToCuppaGameObj[ACTOR.MAGICIAN] = CUPPA_POSE.RIGHT_CUPPA_ARM_DOWN;
			ActorToCuppaGameObj[ACTOR.POLICEMAN] = CUPPA_POSE.LEFT_CUPPA_ARM_DOWN;
			ActorToCuppaGameObj[ACTOR.PUNK] = CUPPA_POSE.FORWARD_STANCE_ARM_DOWN;
            

            cuppaPoses.CurrentPose = GetCurrentCuppaPose();
        }

		public void SetTrigger(ACTOR toAnimate, int stringHash)
		{
			animDict[toAnimate].SetTrigger(stringHash);
		}

		public void BurnActor(ACTOR actor)
		{
			BurnInfo[(int)actor].NonBurnedSprite.enabled = false;
			BurnInfo[(int)actor].BurnedSprite.enabled = true;
			SetVisibilityAdditional(actor, false);

			StartCoroutine("SetOnFire", actor);
		}

		private void SetVisibilityAdditional(ACTOR actor, bool isVisible)
		{
			SpriteRenderer[] additionalSprites = BurnInfo[(int)actor].AdditionalSpritesToDisable;
			for(int i = 0; i < additionalSprites.Length; i++)
			{
				if(isVisible == true)
					additionalSprites[i].enabled = true;
				else
					additionalSprites[i].enabled = false;
			}
		}

		IEnumerator SetOnFire(ACTOR actor)
		{
			FireStruct[(int)actor].FireRenderer.enabled = true;
			AudioManager.Singleton.PlaySFXSound(AudioManager.SFX_ID.BURN_SOUND);
			yield return new WaitForSeconds(1.5f);
			FireStruct[(int)actor].FireRenderer.enabled = false;
		}

		public void UnBurnActor(ACTOR actor)
		{
			BurnInfo[(int)actor].BurnedSprite.enabled = false;
			BurnInfo[(int)actor].NonBurnedSprite.enabled = true;
			SetVisibilityAdditional(actor, true);
		}

		public void SwitchCuppaPoseRandom()
		{
			int MaxPosNum = (int)CUPPA_POSE.MAX_POSES;
			int randomPos = Random.Range(0, MaxPosNum - 1);
			GameObject posGameObjRoot = poseToGameObjDict[cuppaPoses.CurrentPose];
			if(posGameObjRoot.activeInHierarchy)
				posGameObjRoot.SetActive(false);
			cuppaPoses.CurrentPose = (CUPPA_POSE)randomPos;
			posGameObjRoot = poseToGameObjDict[cuppaPoses.CurrentPose];
			posGameObjRoot.SetActive(true);
		}

        private void SetBoiling(CuppaSpritesAnim NewSpriteObj)
        {
            int currBoilingLevel = GameManager.Singleton.CurrentMultiplyer;

            if (currBoilingLevel == 1)
            {
                NewSpriteObj.NonAnimatedSprite.enabled = true;
                if (NewSpriteObj.OptionalArm != null)
                    NewSpriteObj.OptionalArm.enabled = true;
                NewSpriteObj.AnimatedSprite.enabled = false;
                CuppaPosesNew.SteamSpriteRenderer.enabled = false;
            }
            else if (currBoilingLevel == 2)
            {
                // not boiling - use original
                NewSpriteObj.NonAnimatedSprite.enabled = false;
                if (NewSpriteObj.OptionalArm != null)
                    NewSpriteObj.OptionalArm.enabled = true;
                NewSpriteObj.AnimatedSprite.enabled = true;
                NewSpriteObj.BoilingAnimator.speed = AnimationSpeedLevelOne;
                CuppaPosesNew.SteamSpriteRenderer.enabled = false;
            }
            else if (currBoilingLevel == 3)
            {
                // half boiling
                NewSpriteObj.NonAnimatedSprite.enabled = false;
                if (NewSpriteObj.OptionalArm != null)
                    NewSpriteObj.OptionalArm.enabled = true;
                NewSpriteObj.AnimatedSprite.enabled = true;
                NewSpriteObj.BoilingAnimator.speed = AnimationSpeedLevelTwo;
                CuppaPosesNew.SteamSpriteRenderer.enabled = false;
            }
            else if (currBoilingLevel == 4)
            {
                // full boiling
                NewSpriteObj.NonAnimatedSprite.enabled = false;
                if (NewSpriteObj.OptionalArm != null)
                    NewSpriteObj.OptionalArm.enabled = true;
                NewSpriteObj.AnimatedSprite.enabled = true;
                NewSpriteObj.BoilingAnimator.speed = AnimationSpeedLevelThree;
                CuppaPosesNew.SteamSpriteRenderer.enabled = true;
            }
        }


        public void SwitchCuppaPoseForActor(ACTOR currentActor)
        {
            CUPPA_POSE oldPose = cuppaPoses.CurrentPose;

            CuppaSpritesAnim SpriteObj = PosesToSpritesAnimInfo[cuppaPoses.CurrentPose];

            GameObject posGameObjRoot = SpriteObj.RootGameObj;

            //GameObject posGameObjRoot = poseToGameObjDict[cuppaPoses.CurrentPose];
            if (posGameObjRoot.activeInHierarchy)
                posGameObjRoot.SetActive(false);

            cuppaPoses.CurrentPose = ActorToCuppaGameObj[currentActor];
            CuppaSpritesAnim NewSpriteObj = PosesToSpritesAnimInfo[cuppaPoses.CurrentPose];
            posGameObjRoot = NewSpriteObj.RootGameObj;
            //posGameObjRoot = poseToGameObjDict[cuppaPoses.CurrentPose];
            posGameObjRoot.SetActive(true);
            SetBoiling(NewSpriteObj);

            // Update animation for boiling mode
            /////////////////////////////////////////////////////
            // get current animator
            // check current boiling level.  If 0, use default sprite renderer, else use boiling and do the following
            // set speed based on boiling level
            //posesToAnimatorDict
            /////////////////////////////////////////////////////

            // See where joe cuppa was and decide where he moves to based on that

            // Move from left to middle
            if ((oldPose == CUPPA_POSE.LEFT_CUPPA_ARM_DOWN || oldPose == CUPPA_POSE.LEFT_CUPPA_ARM_UP)                      // if old pose was on left
                && (cuppaPoses.CurrentPose == CUPPA_POSE.FORWARD_STANCE_ARM_DOWN || cuppaPoses.CurrentPose == CUPPA_POSE.FORWARD_STANCE_ARM_OUT)    // if new pose is on middle
            )
            {
                //Debug.Log("Triggered move left to mid");
                JoeCuppaAnimator.SetTrigger(StringHashes.JoeCuppaMoveLeftToMid);    // use dictionary?
            }
            else if ((oldPose == CUPPA_POSE.FORWARD_STANCE_ARM_DOWN || oldPose == CUPPA_POSE.FORWARD_STANCE_ARM_OUT)        // if old pose was on middle
                && (cuppaPoses.CurrentPose == CUPPA_POSE.LEFT_CUPPA_ARM_DOWN || cuppaPoses.CurrentPose == CUPPA_POSE.LEFT_CUPPA_ARM_UP) // if new pose is on left
            )
            {
                //Debug.Log("Triggered move mid to left");
                JoeCuppaAnimator.SetTrigger(StringHashes.JoeCuppaMoveMidToLeft);
            }
            else if ((oldPose == CUPPA_POSE.FORWARD_STANCE_ARM_DOWN || oldPose == CUPPA_POSE.FORWARD_STANCE_ARM_OUT)
                && (cuppaPoses.CurrentPose == CUPPA_POSE.RIGHT_CUPPA_ARM_UP || cuppaPoses.CurrentPose == CUPPA_POSE.RIGHT_CUPPA_ARM_DOWN)
            )
            {
                //Debug.Log("Triggered move mid to right");
                JoeCuppaAnimator.SetTrigger(StringHashes.JoeCuppaMoveMidToRight);
            }
            else if ((oldPose == CUPPA_POSE.RIGHT_CUPPA_ARM_UP || oldPose == CUPPA_POSE.RIGHT_CUPPA_ARM_DOWN)
                && (cuppaPoses.CurrentPose == CUPPA_POSE.FORWARD_STANCE_ARM_DOWN || cuppaPoses.CurrentPose == CUPPA_POSE.FORWARD_STANCE_ARM_OUT)
            )
            {
                //Debug.Log("Triggered move right to mid");
                JoeCuppaAnimator.SetTrigger(StringHashes.JoeCuppaMoveRightToMid);   // use dictionary?
            }
            else if ((oldPose == CUPPA_POSE.RIGHT_CUPPA_ARM_UP || oldPose == CUPPA_POSE.RIGHT_CUPPA_ARM_DOWN)
                && (cuppaPoses.CurrentPose == CUPPA_POSE.LEFT_CUPPA_ARM_UP || cuppaPoses.CurrentPose == CUPPA_POSE.LEFT_CUPPA_ARM_DOWN)
            )
            {
                JoeCuppaAnimator.SetTrigger(StringHashes.JoeCuppaMoveRightToLeft);
            }
            else if ((oldPose == CUPPA_POSE.LEFT_CUPPA_ARM_DOWN || oldPose == CUPPA_POSE.LEFT_CUPPA_ARM_UP)
                && (cuppaPoses.CurrentPose == CUPPA_POSE.RIGHT_CUPPA_ARM_UP || cuppaPoses.CurrentPose == CUPPA_POSE.RIGHT_CUPPA_ARM_DOWN)
            )
            {
                JoeCuppaAnimator.SetTrigger(StringHashes.JoeCuppaMoveLeftToRight);
            }
        }

        public void SwitchCuppaPoseForActorOld(ACTOR currentActor)
		{
			CUPPA_POSE oldPose = cuppaPoses.CurrentPose;
			GameObject posGameObjRoot = poseToGameObjDict[cuppaPoses.CurrentPose];
			if(posGameObjRoot.activeInHierarchy)
				posGameObjRoot.SetActive(false);

			cuppaPoses.CurrentPose = ActorToCuppaGameObj[currentActor];
			posGameObjRoot = poseToGameObjDict[cuppaPoses.CurrentPose];
			posGameObjRoot.SetActive(true);

            // Update animation for boiling mode
            /////////////////////////////////////////////////////
            // get current animator
            // check current boiling level.  If 0, use default sprite renderer, else use boiling and do the following
                // set speed based on boiling level
                //posesToAnimatorDict
            /////////////////////////////////////////////////////

            // See where joe cuppa was and decide where he moves to based on that

            // Move from left to middle
            if ((oldPose == CUPPA_POSE.LEFT_CUPPA_ARM_DOWN || oldPose == CUPPA_POSE.LEFT_CUPPA_ARM_UP)						// if old pose was on left
				&& (cuppaPoses.CurrentPose == CUPPA_POSE.FORWARD_STANCE_ARM_DOWN || cuppaPoses.CurrentPose == CUPPA_POSE.FORWARD_STANCE_ARM_OUT)	// if new pose is on middle
			)
			{
				//Debug.Log("Triggered move left to mid");
				JoeCuppaAnimator.SetTrigger(StringHashes.JoeCuppaMoveLeftToMid);	// use dictionary?
			}
			else if((oldPose == CUPPA_POSE.FORWARD_STANCE_ARM_DOWN || oldPose == CUPPA_POSE.FORWARD_STANCE_ARM_OUT)		// if old pose was on middle
				&& (cuppaPoses.CurrentPose == CUPPA_POSE.LEFT_CUPPA_ARM_DOWN || cuppaPoses.CurrentPose == CUPPA_POSE.LEFT_CUPPA_ARM_UP)	// if new pose is on left
			)
			{
				//Debug.Log("Triggered move mid to left");
				JoeCuppaAnimator.SetTrigger(StringHashes.JoeCuppaMoveMidToLeft);
			}
			else if((oldPose == CUPPA_POSE.FORWARD_STANCE_ARM_DOWN || oldPose == CUPPA_POSE.FORWARD_STANCE_ARM_OUT)
				&& (cuppaPoses.CurrentPose == CUPPA_POSE.RIGHT_CUPPA_ARM_UP || cuppaPoses.CurrentPose == CUPPA_POSE.RIGHT_CUPPA_ARM_DOWN)
			)
			{
				//Debug.Log("Triggered move mid to right");
				JoeCuppaAnimator.SetTrigger(StringHashes.JoeCuppaMoveMidToRight);
			}
			else if((oldPose == CUPPA_POSE.RIGHT_CUPPA_ARM_UP || oldPose == CUPPA_POSE.RIGHT_CUPPA_ARM_DOWN)
				&& (cuppaPoses.CurrentPose == CUPPA_POSE.FORWARD_STANCE_ARM_DOWN || cuppaPoses.CurrentPose == CUPPA_POSE.FORWARD_STANCE_ARM_OUT)
			)
			{
				//Debug.Log("Triggered move right to mid");
				JoeCuppaAnimator.SetTrigger(StringHashes.JoeCuppaMoveRightToMid);	// use dictionary?
			}
			else if((oldPose == CUPPA_POSE.RIGHT_CUPPA_ARM_UP || oldPose == CUPPA_POSE.RIGHT_CUPPA_ARM_DOWN)
				&& (cuppaPoses.CurrentPose == CUPPA_POSE.LEFT_CUPPA_ARM_UP || cuppaPoses.CurrentPose == CUPPA_POSE.LEFT_CUPPA_ARM_DOWN)
			)
			{
				//Debug.Log("Triggered move right to left");
				JoeCuppaAnimator.SetTrigger(StringHashes.JoeCuppaMoveRightToLeft);
			}
			else if((oldPose == CUPPA_POSE.LEFT_CUPPA_ARM_DOWN || oldPose == CUPPA_POSE.LEFT_CUPPA_ARM_UP)
				&& (cuppaPoses.CurrentPose == CUPPA_POSE.RIGHT_CUPPA_ARM_UP || cuppaPoses.CurrentPose == CUPPA_POSE.RIGHT_CUPPA_ARM_UP)
			)
			{
				//Debug.Log("Triggered move left to right");
				JoeCuppaAnimator.SetTrigger(StringHashes.JoeCuppaMoveLeftToRight);
			}
		}

        public void UpdateBurnVisualForCurrent()
        {
            SetBoiling(CurrentAnimInfo);
        }

        public void SwitchCuppaPoseToPoint()
        {
            CUPPA_POSE newPose = CUPPA_POSE.FORWARD_STANCE_ARM_DOWN;
            // if left arm down, turn on left arm up
            if (cuppaPoses.CurrentPose == CUPPA_POSE.RIGHT_CUPPA_ARM_DOWN || cuppaPoses.CurrentPose == CUPPA_POSE.RIGHT_CUPPA_ARM_UP)
            {
                newPose = CUPPA_POSE.RIGHT_CUPPA_ARM_UP;
            }
            // if mid arm down, turn on mid arm up
            if (cuppaPoses.CurrentPose == CUPPA_POSE.FORWARD_STANCE_ARM_DOWN || cuppaPoses.CurrentPose == CUPPA_POSE.FORWARD_STANCE_ARM_OUT)
            {
                newPose = CUPPA_POSE.FORWARD_STANCE_ARM_OUT;
            }

            // if right arm down, turn on right arm up
            if (cuppaPoses.CurrentPose == CUPPA_POSE.LEFT_CUPPA_ARM_DOWN || cuppaPoses.CurrentPose == CUPPA_POSE.LEFT_CUPPA_ARM_UP)
            {
                newPose = CUPPA_POSE.LEFT_CUPPA_ARM_UP;
            }

            CuppaSpritesAnim SpriteObj = PosesToSpritesAnimInfo[cuppaPoses.CurrentPose];

            GameObject posGameObjRoot = SpriteObj.RootGameObj;
            if (posGameObjRoot.activeInHierarchy)
                posGameObjRoot.SetActive(false);

            cuppaPoses.CurrentPose = newPose;
            CurrentAnimInfo = PosesToSpritesAnimInfo[cuppaPoses.CurrentPose];

            posGameObjRoot = CurrentAnimInfo.RootGameObj;
            posGameObjRoot.SetActive(true);
            SetBoiling(CurrentAnimInfo);

        }

        public void SwitchCuppaPoseToPointOld()
		{
			CUPPA_POSE newPose = CUPPA_POSE.FORWARD_STANCE_ARM_DOWN;
			// if left arm down, turn on left arm up
			if(cuppaPoses.CurrentPose == CUPPA_POSE.RIGHT_CUPPA_ARM_DOWN  || cuppaPoses.CurrentPose == CUPPA_POSE.RIGHT_CUPPA_ARM_UP)
			{
				newPose = CUPPA_POSE.RIGHT_CUPPA_ARM_UP;
			}
			// if mid arm down, turn on mid arm up
			if(cuppaPoses.CurrentPose == CUPPA_POSE.FORWARD_STANCE_ARM_DOWN || cuppaPoses.CurrentPose == CUPPA_POSE.FORWARD_STANCE_ARM_OUT)
			{
				newPose = CUPPA_POSE.FORWARD_STANCE_ARM_OUT;
			}

			// if right arm down, turn on right arm up
			if(cuppaPoses.CurrentPose == CUPPA_POSE.LEFT_CUPPA_ARM_DOWN || cuppaPoses.CurrentPose == CUPPA_POSE.LEFT_CUPPA_ARM_UP)
			{
				newPose = CUPPA_POSE.LEFT_CUPPA_ARM_UP;
			}

			// set it
			GameObject posGameObjRoot = poseToGameObjDict[cuppaPoses.CurrentPose];
			if(posGameObjRoot.activeInHierarchy)
				posGameObjRoot.SetActive(false);

			cuppaPoses.CurrentPose = newPose;
			posGameObjRoot = poseToGameObjDict[cuppaPoses.CurrentPose];
			posGameObjRoot.SetActive(true);
		}
	}
}
