﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System.Collections;
using System.Collections.Generic;
using Assets.Data;

namespace Assets.Managers
{
	public class JokeManager : MonoBehaviour {

		public static JokeManager Singleton = null;

		public Button JokeSetUpButton = null;
		Text JokeSetUpText = null;

		public Button[] OptionButtons = null;
		List<Text> OptionButtonText = new List<Text>();

		const string MissingWordLabel = "______";

		int CurrentCorrectIndex = 0;


		[System.Serializable]
		public class RobotSpotlight
		{
			public Light ComedySpotlight;
			public ACTOR RobotType;
		}
		public RobotSpotlight[] lightArray;
		RobotSpotlight CurrentRobotSpotlight = null;

		[System.Serializable]
		public class RobotSpeachBubble
		{
			public GameObject SpeachBubble;
			public TextMesh SpeachText;
			public ACTOR RobotType;
		}

		public RobotSpeachBubble[] SpeachBubbleArray;
		RobotSpeachBubble CurrentSpeachBubble;

		[System.Serializable]
		public class Joke
		{
			public string RobotHeckle;
			public string JoeCuppaSetUp_pOne;
			public string JoeCuppaSetUp_pTwo;
			public string WinningChoice;
			public string BadChoiceOne;
			public string BadChoiceTwo;
			public string BadChoiceThree;
			public int index;
			public ACTOR Actor;

			public Joke(ACTOR inActor)
			{
				Actor = inActor;
			}

			public Joke(string heckle, string setup_pOne, string setup_pTwo, string winningChoice,
				string badChoiceOne, string badChoiceTwo, string badChoicethree, int inIndex, ACTOR inActor)
			{
				RobotHeckle = heckle;
				JoeCuppaSetUp_pOne = setup_pOne;
				JoeCuppaSetUp_pTwo = setup_pTwo;
				WinningChoice = winningChoice;
				BadChoiceOne = badChoiceOne;
				BadChoiceTwo = badChoiceTwo;
				BadChoiceThree = badChoicethree;
				index = inIndex;
				Actor = inActor;
			}
		}

		List<ACTOR> AvailableTypes = new List<ACTOR>();

		//List<Joke> JokeList = ;
		Dictionary<ACTOR, List<Joke>> RobotToJokeDict = new Dictionary<ACTOR, List<Joke>>();

		/// HACK TO FIX ANNOYING ISSUE WITH BUTTON COLOR
		Color OriginalButtonColor;

		///

		// Use this for initialization
		void Start () {
			if(Singleton == null)
			{
				Singleton = this;

				for(int i = 0; i < OptionButtons.Length; i++)
				{
					Text currentTextItem = OptionButtons[i].GetComponentInChildren<Text>();
					OptionButtonText.Add(currentTextItem);
				}
				JokeSetUpText = JokeSetUpButton.GetComponentInChildren<Text>();
				Singleton.InitDictionary();
				AvailableTypes.Add(ACTOR.ASTRONAUT);
                
				AvailableTypes.Add(ACTOR.BALLERINA);
				AvailableTypes.Add(ACTOR.FARMER);
				AvailableTypes.Add(ACTOR.FISHERMAN);
				AvailableTypes.Add(ACTOR.MAGICIAN);
				AvailableTypes.Add(ACTOR.DINOSAUR);
				AvailableTypes.Add(ACTOR.GHOST);
				AvailableTypes.Add(ACTOR.POLICEMAN);
				AvailableTypes.Add(ACTOR.PUNK);
                
				OptionButtons[0].onClick.AddListener(() => OptionButtonOneCallback());
				OptionButtons[1].onClick.AddListener(() => OptionButtonTwoCallback());
				OptionButtons[2].onClick.AddListener(() => OptionButtonThreeCallback());
				OptionButtons[3].onClick.AddListener(() => OptionButtonFourCallback());

				OriginalButtonColor = OptionButtons[0].colors.normalColor;
				DisableUI();
			}
		}

		public bool CheckIfJokeIsCorrect(int buttonIndex)
		{
			return buttonIndex == CurrentCorrectIndex ? true : false;
		}

		public void ShowUI()
		{

			if(!JokeSetUpButton.gameObject.activeInHierarchy)
			{
				JokeSetUpButton.gameObject.SetActive(true);
				for(int i = 0; i < OptionButtons.Length; i++)
				{
					OptionButtons[i].gameObject.SetActive(true);
				}
				if(GameManager.Singleton != null)
					GameManager.Singleton.EnableTimerVisuals();
			}
		}

		public void SetUIWithJoke(Joke newJoke)
		{
			ShowUI();

            // TODO(Ferreira): Issue with dog here
            
			if(newJoke.JoeCuppaSetUp_pTwo.CompareTo("!") != 0)
			{
				JokeSetUpText.text = newJoke.JoeCuppaSetUp_pOne + MissingWordLabel + " " + newJoke.JoeCuppaSetUp_pTwo;
			}
			else
			{
				JokeSetUpText.text = newJoke.JoeCuppaSetUp_pOne + MissingWordLabel + newJoke.JoeCuppaSetUp_pTwo;
			}
			// randomize options
			CurrentCorrectIndex = Random.Range(0, OptionButtons.Length);

			int numOptionsUsed = 0;

			for(int i = 0; i < OptionButtons.Length; i++)
			{
				if( i == CurrentCorrectIndex )
				{
					OptionButtonText[i].text = newJoke.WinningChoice;
				}
				else
				{
					int numOptionsLeft = OptionButtons.Length - numOptionsUsed;
					switch(numOptionsLeft)
					{
					case 2:
						OptionButtonText[i].text = newJoke.BadChoiceThree;		// Would be the power slide option
						break;
					case 3:
						OptionButtonText[i].text = newJoke.BadChoiceTwo;
						break;
					case 4:
						OptionButtonText[i].text = newJoke.BadChoiceOne;
						break;
					}
					numOptionsUsed++;
				}
			}
			DisableUI();
		}

		public void DisableUI()
		{
			if(JokeSetUpButton.gameObject.activeInHierarchy)
			{
				JokeSetUpButton.gameObject.SetActive(false);
				for(int i = 0; i < OptionButtons.Length; i++)
				{
					OptionButtons[i].gameObject.SetActive(false);
				}

				if(GameManager.Singleton != null)
					GameManager.Singleton.DisableTimerVisuals();
			}
		}

		public void OpenSpeachBubble(ACTOR actorType, string textToDisplay)
		{
			//if(CurrentSpeachBubble != null)
			//	CurrentSpeachBubble.SpeachBubble.SetActive(false);
			CurrentRobotSpotlight = lightArray[(int)actorType];
			CurrentRobotSpotlight.ComedySpotlight.enabled = true;

			CurrentSpeachBubble = SpeachBubbleArray[(int)actorType];
			CurrentSpeachBubble.SpeachBubble.SetActive(true);
			CurrentSpeachBubble.SpeachText.text = textToDisplay;
		}

		public void CloseSpeachBubble()
		{
			//if(CurrentSpeachBubble != null)
			CurrentSpeachBubble.SpeachBubble.SetActive(false);
		}

        public bool IsOutOfJokes()
        {
            for(int i = 0; i < AvailableTypes.Count; i++)
            {
                for(int j = 0; j < RobotToJokeDict[(ACTOR)i].Count; j++)
                {
                    //if()
                }

            }
            return false;
        }

        public Joke GetRandomJoke()
        {
            Joke chosenJoke = null;
            if (CurrentRobotSpotlight != null)
                CurrentRobotSpotlight.ComedySpotlight.enabled = false;

            // First do a random of a count of the dict
            // Use that to get the type

            if (RobotToJokeDict.Count > 0)
            {
                int dictIndex = Random.Range(0, AvailableTypes.Count);

                // if no elements exist for that item, do a search
                ACTOR currentTypeToPullFrom = (ACTOR)AvailableTypes[dictIndex];

                List<Joke> currJokeList = RobotToJokeDict[currentTypeToPullFrom];

                int indexToPullFrom = Random.Range(0, currJokeList.Count);
                chosenJoke = currJokeList[indexToPullFrom];

                // Remove the joke permanently
                currJokeList.Remove(chosenJoke);

                // If there are no more jokes in that category, remove the category so it is not chosen again
                if (currJokeList.Count == 0)
                {

                    RobotToJokeDict.Remove(currentTypeToPullFrom);
                    AvailableTypes.Remove(currentTypeToPullFrom);
                }
            }
            else
            {
                GameManager.Singleton.Lose();
            }

            return chosenJoke;
        }

        public Joke GetRandomJokeOld()
		{
			Joke chosenJoke = null;
			if(CurrentRobotSpotlight != null)
				CurrentRobotSpotlight.ComedySpotlight.enabled = false;

            // First do a random of a count of the dict
            // Use that to get the type

			if(RobotToJokeDict.Count > 0)
			{
				int dictIndex = Random.Range(0, AvailableTypes.Count);

				// if no elements exist for that item, do a search
				ACTOR currentTypeToPullFrom = (ACTOR)AvailableTypes[dictIndex];
				//foreach(ACTOR actor in AvailableTypes)
				//{
					//Debug.Log("actor : " + actor);
				//}

				// Get a random element from that index
				//Assert.AreNotEqual(0, RobotToJokeDict[currentTypeToPullFrom].Count);

				if(currentTypeToPullFrom == ACTOR.DOG)
				{
					chosenJoke = new Joke(ACTOR.DOG);
				}
				else if(RobotToJokeDict[currentTypeToPullFrom].Count > 0)
                {
                    Debug.Log(currentTypeToPullFrom + "->Count->" + RobotToJokeDict[currentTypeToPullFrom].Count);
                    int indexToPullFrom = Random.Range(0, RobotToJokeDict[currentTypeToPullFrom].Count - 1);
                    Assert.AreNotEqual(indexToPullFrom, RobotToJokeDict[currentTypeToPullFrom].Count - 1);
					chosenJoke = RobotToJokeDict[currentTypeToPullFrom][indexToPullFrom];


					// Remove the joke permanently
					RobotToJokeDict[currentTypeToPullFrom].Remove(chosenJoke);

					// If there are no more jokes in that category, remove the category so it is not chosen again
					if(RobotToJokeDict[currentTypeToPullFrom].Count == 0)
					{

						RobotToJokeDict.Remove(currentTypeToPullFrom);
						AvailableTypes.Remove(currentTypeToPullFrom);

						/////////// IMPORTANT ///////////////////
						/// 
						///  KEEP A LIST OF THE AVAILABLE KEYS AND REMOVE THEM ONCE REMOVED FROM THIS LIST
						/// 
						/////////// IMPORTANT ///////////////////
					}
				}
			}

			return chosenJoke;
		}

		public Joke GetJokeForType(ACTOR type)
		{
			Debug.Log("Got into joke getter");
			Assert.AreNotEqual(0, RobotToJokeDict[type].Count);
			int indexToPullFrom = Random.Range(0, RobotToJokeDict[type].Count - 1);
			Joke chosenJoke = RobotToJokeDict[type][indexToPullFrom];
			RobotToJokeDict[type].Remove(chosenJoke);
			return chosenJoke;
		}

		void InitDictionary()
		{
			/* DEBUGGING
			RobotToJokeDict[RobotType.ASTRONAUT] = new List<Joke>();
			RobotToJokeDict[RobotType.FARMER] = new List<Joke>();
			RobotToJokeDict[RobotType.FISHERMAN] = new List<Joke>();
			RobotToJokeDict[RobotType.MAGICIAN] = new List<Joke>();
			RobotToJokeDict[RobotType.BALLERINA] = new List<Joke>();

			RobotToJokeDict[RobotType.ASTRONAUT].Add(new Joke("AstroTest", "Astro", "time!", "AstroWin", "AstroBad1", "AstroBad2", "AstroBad3"));
			RobotToJokeDict[RobotType.FARMER].Add(new Joke("FarmerTest", "Farmer", "time!", "FarmerWin", "Farmer1", "Farmer2", "Farmer3"));
			RobotToJokeDict[RobotType.FISHERMAN].Add(new Joke("Fisherman", "Fisherman", "time!", "FishermanWin", "FishermanBad1", "FishermanBad2", "FishermanBad3"));
			RobotToJokeDict[RobotType.MAGICIAN].Add(new Joke("Magician Test", "Magician", "time!", "MagicianWin", "MagicianBad1", "MagicianBad2", "Magician3"));
			RobotToJokeDict[RobotType.BALLERINA].Add(new Joke("Ballerina Test", "Ballerina", "time!", "BallerinaWin", "BallerinaBad1", "BallerinaBad2", "BallerinaBad3"));
			*/

			RobotToJokeDict[ACTOR.ASTRONAUT] = new List<Joke>();
			RobotToJokeDict[ACTOR.FARMER] = new List<Joke>();
			RobotToJokeDict[ACTOR.FISHERMAN] = new List<Joke>();
			RobotToJokeDict[ACTOR.MAGICIAN] = new List<Joke>();
			RobotToJokeDict[ACTOR.BALLERINA] = new List<Joke>();
			RobotToJokeDict[ACTOR.POLICEMAN] = new List<Joke>();
			RobotToJokeDict[ACTOR.GHOST] = new List<Joke>();
			RobotToJokeDict[ACTOR.PUNK] = new List<Joke>();
			RobotToJokeDict[ACTOR.DINOSAUR] = new List<Joke>();
			RobotToJokeDict[ACTOR.DOG] = new List<Joke>();
            
			// im hungry -> your act doesn't have much pull
			// im sick -> home
			// terribly -> sick

			RobotToJokeDict[ACTOR.ASTRONAUT].Add(new Joke("I’m sick of listening to these jokes… I’m hungry!", "Well, it’s almost ", "time!", "Launch", "Party", "Duck", "Power Slide", 0, ACTOR.ASTRONAUT));
			RobotToJokeDict[ACTOR.ASTRONAUT].Add(new Joke("You’re doing terribly up there!", "Hey now, I’m trying my best to ", "!", "Rocket", "Grow", "Duck", "Speak", 1, ACTOR.ASTRONAUT));
			RobotToJokeDict[ACTOR.ASTRONAUT].Add(new Joke("Your act doesn’t have much pull!", "I don't think you understand the ", "of the situation!", "Gravity", "Point", "Drive", "School", 2, ACTOR.ASTRONAUT));
			RobotToJokeDict[ACTOR.ASTRONAUT].Add(new Joke("I’ll send you home for these jokes!", "And I'll send you to the ", "!", "Moon", "Earth", "Beach", "Park", 3, ACTOR.ASTRONAUT));
			RobotToJokeDict[ACTOR.ASTRONAUT].Add(new Joke("Give me a break!", "Give me some ", "!", "Space", "Food", "Time", "Friends", 4, ACTOR.ASTRONAUT));


            RobotToJokeDict[ACTOR.FARMER].Add(new Joke("I'm gonna punch you in the face!", "And I'm gonna ", "in yours!", "Laugh", "Run", "Cry", "Smell", 0, ACTOR.FARMER));
			RobotToJokeDict[ACTOR.FARMER].Add(new Joke("You're asking to get wrangled!", "And you're asking for ", "to the face!", "Hot coffee", "Cold milk", "Juice", "A train", 1, ACTOR.FARMER));
			RobotToJokeDict[ACTOR.FARMER].Add(new Joke("My cows aren’t afraid to tell jokes!", "I don't know, I've heard they're ", "!", "Cowards", "Strong", "Friendly", "Beautiful", 2, ACTOR.FARMER));
			RobotToJokeDict[ACTOR.FARMER].Add(new Joke("I’ll send my pigs to attack you!", "The only attacks they know are ", "!", "Pork Chops", "Cow Punches", "Duck Slides", "Donkey Kicks", 3, ACTOR.FARMER));
            RobotToJokeDict[ACTOR.FARMER].Add(new Joke("My horses will find you in the night!", "I hope they don't give me ", "!", "Nightmares", "Good dreams", "Night... something", "Nightducks", 4, ACTOR.FARMER));

            RobotToJokeDict[ACTOR.FISHERMAN].Add(new Joke("Your jokes are bad and you should feel bad!", "Something in here smells ", "!", "Fishy", "Like a dog", "Bad", "Chickeny", 0, ACTOR.FISHERMAN));
			RobotToJokeDict[ACTOR.FISHERMAN].Add(new Joke("I’m not paying for this performance!", "That's OK, you'd only pay in ", "anyway!", "Sand dollars", "Dollar bills", "Loose change", "Mean words", 1, ACTOR.FISHERMAN));
			RobotToJokeDict[ACTOR.FISHERMAN].Add(new Joke("I can’t believe I spent money to hear these jokes.", "Guess you'd better catch a ", "!", "Goldfish", "Whale", "Disease", "Ball", 2, ACTOR.FISHERMAN));
			RobotToJokeDict[ACTOR.FISHERMAN].Add(new Joke("I don’t like you OR your friends!", "Why are you so ", "?", "Crabby", "Silly", "Funny", "Mad", 3, ACTOR.FISHERMAN));
            RobotToJokeDict[ACTOR.FISHERMAN].Add(new Joke("These jokes are sub par!", "I'm sorry, it's not like I'm memorizing ", "!", "Lines", "Music", "Books", "Fish jokes", 4, ACTOR.FISHERMAN));

            RobotToJokeDict[ACTOR.MAGICIAN].Add(new Joke("This is making me angry!", "Don't pull your ", "out!", "Hare", "Cat", "Grandmother", "Toes", 0, ACTOR.MAGICIAN));
			RobotToJokeDict[ACTOR.MAGICIAN].Add(new Joke("This isn’t very impressive.", "You're supposed to be ", "!", "Impressto", "Happy", "Confused", "Something", 1, ACTOR.MAGICIAN));
			RobotToJokeDict[ACTOR.MAGICIAN].Add(new Joke("This comedy isn’t anything special.", "You're right, it's ", "!", "Magical", "Weird", "Silly", "Elegant", 2, ACTOR.MAGICIAN));
			RobotToJokeDict[ACTOR.MAGICIAN].Add(new Joke("Quick! Look behind you!", "You can't ", "me!", "Trick", "Treat", "Hurt", "Push", 3, ACTOR.MAGICIAN));
            RobotToJokeDict[ACTOR.MAGICIAN].Add(new Joke("You are just the worst!", "Too bad you brought a ", "to a word fight!", "Wand", "Duck", "Buddy", "Video game", 4, ACTOR.MAGICIAN));

            RobotToJokeDict[ACTOR.BALLERINA].Add(new Joke("I find your jokes offensive!", "I like putting people off ", "!", "Balance", "Target", "Themselves", "Logic", 0, ACTOR.BALLERINA));
			RobotToJokeDict[ACTOR.BALLERINA].Add(new Joke("This comedy is far lazier than my dancing!", "I dunno, I think it's pretty good for your ", "!", "Soles", "Friends", "Mother", "Duck", 1, ACTOR.BALLERINA));
			RobotToJokeDict[ACTOR.BALLERINA].Add(new Joke("That joke wasn’t funny at all!", "I think you're ", "around the issue here!", "Dancing", "Walking", "Talking", "Spanking", 2, ACTOR.BALLERINA));
			RobotToJokeDict[ACTOR.BALLERINA].Add(new Joke("I dislike you!", "I dislike you ", "!", "Tutu", "Sometimes", "I think?", "Not", 3, ACTOR.BALLERINA));
            RobotToJokeDict[ACTOR.BALLERINA].Add(new Joke("Booooooo!", "Your sense of humor needs to be more ", "!", "Flexible", "Bouncy", "Strange", "Interesting", 4, ACTOR.BALLERINA));

            RobotToJokeDict[ACTOR.POLICEMAN].Add(new Joke("You need to obey the law!", "The law needs to obey ", "!", "Me", "You", "The sky", "A duck", 0, ACTOR.POLICEMAN));
			RobotToJokeDict[ACTOR.POLICEMAN].Add(new Joke("You seem suspicious!", "Get out of here, Robo ", "!", "Cop", "Farmer", "Astronaut", "Mom", 1, ACTOR.POLICEMAN));
			RobotToJokeDict[ACTOR.POLICEMAN].Add(new Joke("I'm here to police this situation!", "Serve and protect my ", "!", "Drinks", "Book", "Question", "Radio", 2, ACTOR.POLICEMAN));
			RobotToJokeDict[ACTOR.POLICEMAN].Add(new Joke("Your jokes are in bad taste!", "Next time, I'll tell ", "!", "Donuts", "Jokes", "The sky", "Myself", 3, ACTOR.POLICEMAN));
            RobotToJokeDict[ACTOR.POLICEMAN].Add(new Joke("You tell your jokes too slowly!", "Better than driving too ", "!", "Fast", "Much", "Often", "Strangely", 4, ACTOR.POLICEMAN));

            RobotToJokeDict[ACTOR.GHOST].Add(new Joke("Wow. You are really only making me fall asleep.", "You know what helps with that? ", "!", "I scream", "Loud noises", "Ducks", "Sick beats", 0, ACTOR.GHOST));
			RobotToJokeDict[ACTOR.GHOST].Add(new Joke("Dude, I will literally fight you.", "Careful, if you do you may get a ", "!", "Boo-boo", "Pain", "Friend", "Family member", 1, ACTOR.GHOST));
			RobotToJokeDict[ACTOR.GHOST].Add(new Joke("I should really be out shopping instead of here.", "Where do you shop? At the ", "store?", "Ghostery", "Spooky", "Stinky", "Store store", 2, ACTOR.GHOST));
			RobotToJokeDict[ACTOR.GHOST].Add(new Joke("I'm just gonna drive home now.", "Make sure you don't get lost in any ", "!", "Dead ends", "Streets", "Confusing roads", "Maps", 3, ACTOR.GHOST));
            RobotToJokeDict[ACTOR.GHOST].Add(new Joke("You're trying, but you're blowing it, man.", "Don't speak unless ", "to!", "Spooken", "Persuaded", "Talked", "Instructed", 4, ACTOR.GHOST));

            RobotToJokeDict[ACTOR.PUNK].Add(new Joke("Hey, man, you smell bad!", "You smell ", "!", "Worse", "Good", "Pleasant", "Decent", 0, ACTOR.PUNK));
			RobotToJokeDict[ACTOR.PUNK].Add(new Joke("Hey bro, the coffee tastes terrible!", "Better than the ", "you cook!", "Food", "People", "Coffee", "Walls", 1, ACTOR.PUNK));
			RobotToJokeDict[ACTOR.PUNK].Add(new Joke("Dude, you need to tell better jokes!", "And you need to look less ", "!", "Ugly", "Pretty", "Cool", "Friendly", 2, ACTOR.PUNK));
			RobotToJokeDict[ACTOR.PUNK].Add(new Joke("You don't know what my life is like, man!", "I know you're a total ", "!", "Stinker", "Cool person", "Cat", "Cake", 3, ACTOR.PUNK));
            RobotToJokeDict[ACTOR.PUNK].Add(new Joke("Your outfit makes you look like a square!", "Better than like a ", "!", "Doofus", "...Woah!", "Person", "Triangle", 4, ACTOR.PUNK));

            RobotToJokeDict[ACTOR.DINOSAUR].Add(new Joke("ROAR!", "You're ", "!", "Extinct", "Green", "Beautiful", "Good at math", 0, ACTOR.DINOSAUR));
            RobotToJokeDict[ACTOR.DINOSAUR].Add(new Joke("RARRRRR!", "Your brain is ", "!", "Small", "Big", "Carrot", "Smart", 1, ACTOR.DINOSAUR));
            RobotToJokeDict[ACTOR.DINOSAUR].Add(new Joke("BRAAAAAAAAAAAP!", "People study your ", "!", "Bones", "Uncle", "...Dunno.", "Bed", 2, ACTOR.DINOSAUR));
            RobotToJokeDict[ACTOR.DINOSAUR].Add(new Joke("GRRRRRRRRR!", "You belong in a ", "!", "Museum", "Bakery", "Library", "Swimming pool", 3, ACTOR.DINOSAUR));
            RobotToJokeDict[ACTOR.DINOSAUR].Add(new Joke("MRAWRRRRR!", "You can't ", "!", "Carry things", "Make loud noises", "Walk", "Eat", 4, ACTOR.DINOSAUR));

        }

        void ResetButtonColors()
		{
			for(int i = 0; i < OptionButtons.Length; i++)
			{
				OptionButtons[i].image.color = OriginalButtonColor;
			}
		}

		IEnumerator IncorrectAnswer()
		{

			DisableUI();
			GameManager.Singleton.ChooseWrongJoke();
			yield return new WaitForSeconds(1.0f);
			ShowUI();
		}

		void OptionButtonOneCallback()
		{
			int myIndex = 0;
			if(CheckIfJokeIsCorrect(myIndex) == true)
			{
				//GameManager.Singleton.ChooseRightJoke();
				GameManager.Singleton.CurrentState = STATE.CORRECT_RESPONSE;
			}
			else
			{
				StartCoroutine("IncorrectAnswer");
			}
			ResetButtonColors();
		}


		void OptionButtonTwoCallback()
		{
			int myIndex = 1;
			if(CheckIfJokeIsCorrect(myIndex) == true)
			{
				//GameManager.Singleton.ChooseRightJoke();
				GameManager.Singleton.CurrentState = STATE.CORRECT_RESPONSE;
			}
			else
			{
				StartCoroutine("IncorrectAnswer");
			}
			ResetButtonColors();
		}


		void OptionButtonThreeCallback()
		{
			int myIndex = 2;
			if(CheckIfJokeIsCorrect(myIndex) == true)
			{
				GameManager.Singleton.CurrentState = STATE.CORRECT_RESPONSE;
			}
			else
			{
				StartCoroutine("IncorrectAnswer");
			}
			ResetButtonColors();
		}

		void OptionButtonFourCallback()
		{
			int myIndex = 3;
			if(CheckIfJokeIsCorrect(myIndex) == true)
			{
				GameManager.Singleton.CurrentState = STATE.CORRECT_RESPONSE;
			}
			else
			{
				StartCoroutine("IncorrectAnswer");
			}
			ResetButtonColors();
		}

	}
}