﻿using UnityEngine;
using System.Collections;

namespace Assets.Managers
{
	public class BadassTextManager : MonoBehaviour {

		public static BadassTextManager Singleton = null;
		public Transform SpawnPosition;

		public enum TEXT_ELEMENT
		{
			SICK_BURN,
			TOASTED,
			NUM_TEXT_ELEMENTS
		}

		[System.Serializable]
		public class SwoopyText
		{
			public GameObject SwoopyTextPrefab;
			public int LevelInHierarchy;
			public string CorrespondingText;
		}

		public SwoopyText[] SwoopyTextPrefabs;

		// Use this for initialization
		void Start () {
			if(Singleton == null)
			{
				Singleton = this;
			}
		}


		public TEXT_ELEMENT GetRandomTextelement()
		{
			int randomNum = Random.Range(0, (int)TEXT_ELEMENT.NUM_TEXT_ELEMENTS);
			return (TEXT_ELEMENT) randomNum;
		}

		public void SpawnText(TEXT_ELEMENT textToSpawn)
		{
			GameObject spawnedText = Instantiate(SwoopyTextPrefabs[(int)textToSpawn].SwoopyTextPrefab, SpawnPosition.position, Quaternion.identity) as GameObject;
			Destroy(spawnedText, 5.0f);
		}
	}
}