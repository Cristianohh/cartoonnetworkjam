﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;
using System.Collections.Generic;
using Assets.Data;

namespace Assets.Managers
{
	public class AudioManager : MonoBehaviour {

		public static AudioManager Singleton;

		AudioSource[] AudioSources;

		List<AudioClip> PositiveRiffClips = new List<AudioClip>();
		List<AudioClip> NegativeRiffClips = new List<AudioClip>();


		public enum AUDIO_SOURCE_ID {
			SFX,
			RIFF_SOURCE,
			VOICE_ACTING,
			MUSIC_SOURCE,
			CROWD_SOURCE,
            BOILING_SOURCE,
			MAX_SFX_CLIP
		};

		class Clips
		{
			public AudioClip Heckle;
			public AudioClip Joke;
			public AudioClip Setup;
			//public int index;

			public Clips(string HeckleLoadLoc, string JokeLoadLoc, string SetupLoadLoc)
			{
				Heckle = Resources.Load(HeckleLoadLoc, typeof(AudioClip)) as AudioClip;
				Joke = Resources.Load(JokeLoadLoc, typeof(AudioClip)) as AudioClip;
				Setup = Resources.Load(SetupLoadLoc, typeof(AudioClip)) as AudioClip;
			}
		};

		public enum MUSIC_ID
		{
			INTRO_LOOP,
			MAIN_LOOP,
			WIN_MUSIC,
			LOSE_MUSIC,
			NUM_LOOPS
		};

		public enum SFX_ID
		{
			BURN_SOUND,
			MENU_CLICK,
			CROWD_CHEER,
			CROWD_LAUGH,
			CROWD_BOO,
            DOG_BLINK,
            TEA_KETTLE,
            BOILING_SOUND,
            NUM_SOUNDS
		};
        

		Dictionary<MUSIC_ID, AudioClip> musicClipDict = new Dictionary<MUSIC_ID, AudioClip>();
		Dictionary<SFX_ID, AudioClip> sfxClipDict = new Dictionary<SFX_ID, AudioClip>();

		Dictionary<ACTOR, List<Clips>> narrationClipDict = new Dictionary<ACTOR, List<Clips>>();
		const int NumClipsPerActor = 5;

		// Use this for initialization
		void Start () {
			if(Singleton == null)
			{
				Singleton = this;
				AudioSources = gameObject.GetComponentsInChildren<AudioSource>();
				Assert.AreNotEqual(0, AudioSources.Length);
				int numRiffClips = 9;

				// Load positive riff clips
				for(int i = 1; i < numRiffClips; i++)
				{
					AudioClip currclip = Resources.Load("Audio/SFX/Riffs/Positive/guitar_burn_" + i,typeof(AudioClip)) as AudioClip;
					PositiveRiffClips.Add(currclip);
				}
				Assert.AreEqual(numRiffClips, PositiveRiffClips.Count + 1);

				// Load negative riff clips
				numRiffClips = 8;
				for(int i = 1; i < numRiffClips; i++)
				{
					AudioClip currclip = Resources.Load("Audio/SFX/Riffs/Negative/burnfail_" + i,typeof(AudioClip)) as AudioClip;
					NegativeRiffClips.Add(currclip);
				}
				Assert.AreEqual(numRiffClips, NegativeRiffClips.Count + 1);

				narrationClipDict[ACTOR.ASTRONAUT] = new List<Clips>();
				narrationClipDict[ACTOR.FARMER] = new List<Clips>();
				narrationClipDict[ACTOR.FISHERMAN] = new List<Clips>();
				narrationClipDict[ACTOR.MAGICIAN] = new List<Clips>();
				narrationClipDict[ACTOR.BALLERINA] = new List<Clips>();
				narrationClipDict[ACTOR.POLICEMAN] = new List<Clips>();
				narrationClipDict[ACTOR.GHOST] = new List<Clips>();
				narrationClipDict[ACTOR.PUNK] = new List<Clips>();
				narrationClipDict[ACTOR.DINOSAUR] = new List<Clips>();

				for (int i = 1; i <= (NumClipsPerActor); i++)
                {
					//int i = index+1;
                    narrationClipDict[ACTOR.ASTRONAUT].Add(new Clips("Audio/Narration/DX_astronaut_heckle_" + i,
                        "Audio/Narration/DX_astronaut_joke_" + i, "Audio/Narration/DX_astronaut_setup_" + i));
                    narrationClipDict[ACTOR.FARMER].Add(new Clips("Audio/Narration/DX_farmer_heckle_" + i,
                        "Audio/Narration/DX_farmer_joke_" + i, "Audio/Narration/DX_farmer_setup_" + i));
                    narrationClipDict[ACTOR.FISHERMAN].Add(new Clips("Audio/Narration/DX_fish_heckle_" + i,
                        "Audio/Narration/DX_fish_joke_" + i, "Audio/Narration/DX_fish_setup_" + i));
                    narrationClipDict[ACTOR.MAGICIAN].Add(new Clips("Audio/Narration/DX_magician_heckle_" + i,
                        "Audio/Narration/DX_magician_joke_" + i, "Audio/Narration/DX_magician_setup_" + i));
                    narrationClipDict[ACTOR.BALLERINA].Add(new Clips("Audio/Narration/DX_dancer_heckle_" + i,
                        "Audio/Narration/DX_dancer_joke_" + i, "Audio/Narration/DX_dancer_setup_" + i));
                    narrationClipDict[ACTOR.POLICEMAN].Add(new Clips("Audio/Narration/DX_cop_heckle_" + i,
                        "Audio/Narration/DX_cop_joke_" + i, "Audio/Narration/DX_cop_setup_" + i));
                    narrationClipDict[ACTOR.GHOST].Add(new Clips("Audio/Narration/DX_ghost_heckle_" + i,
                        "Audio/Narration/DX_ghost_joke_" + i, "Audio/Narration/DX_ghost_setup_" + i));
                    narrationClipDict[ACTOR.PUNK].Add(new Clips("Audio/Narration/DX_punk_heckle_" + i,
                        "Audio/Narration/DX_punk_joke_" + i, "Audio/Narration/DX_punk_setup_" + i));
                    narrationClipDict[ACTOR.DINOSAUR].Add(new Clips("Audio/Narration/DX_dino_heckle_" + i,
                        "Audio/Narration/DX_dino_joke_" + i, "Audio/Narration/DX_dino_setup_" + i));
                }
					
				musicClipDict[MUSIC_ID.INTRO_LOOP] = Resources.Load("Audio/Music/openingscene_loop",typeof(AudioClip)) as AudioClip;
				musicClipDict[MUSIC_ID.MAIN_LOOP] = Resources.Load("Audio/Music/comedyclub_loop",typeof(AudioClip)) as AudioClip;
				musicClipDict[MUSIC_ID.WIN_MUSIC] = Resources.Load("Audio/Music/cuppa_wins",typeof(AudioClip)) as AudioClip;
				musicClipDict[MUSIC_ID.LOSE_MUSIC] = Resources.Load("Audio/Music/losing_sting",typeof(AudioClip)) as AudioClip;

				sfxClipDict[SFX_ID.BURN_SOUND] = Resources.Load("Audio/SFX/burn-01",typeof(AudioClip)) as AudioClip;
				sfxClipDict[SFX_ID.MENU_CLICK] = Resources.Load("Audio/SFX/menu",typeof(AudioClip)) as AudioClip;
				sfxClipDict[SFX_ID.CROWD_CHEER] = Resources.Load("Audio/SFX/cheering",typeof(AudioClip)) as AudioClip;
				sfxClipDict[SFX_ID.CROWD_LAUGH] = Resources.Load("Audio/SFX/laughing",typeof(AudioClip)) as AudioClip;
                sfxClipDict[SFX_ID.CROWD_BOO] = Resources.Load("Audio/SFX/booing", typeof(AudioClip)) as AudioClip;
                sfxClipDict[SFX_ID.DOG_BLINK] = Resources.Load("Audio/SFX/dog_blink", typeof(AudioClip)) as AudioClip;
                sfxClipDict[SFX_ID.TEA_KETTLE] = Resources.Load("Audio/SFX/teaKettle", typeof(AudioClip)) as AudioClip;
                sfxClipDict[SFX_ID.BOILING_SOUND] = Resources.Load("Audio/SFX/boiling", typeof(AudioClip)) as AudioClip;
            }
        }

        public void SwitchLoopingForSource(AUDIO_SOURCE_ID src, bool isLooping)
        {
            AudioSources[(int)src].loop = isLooping;
        }
        public void StopAudioForSource(AUDIO_SOURCE_ID src)
        {
            AudioSources[(int)src].Stop();
        }

        public void SetAudioSourceVol(AUDIO_SOURCE_ID src, float vol)
        {
            AudioSources[(int)src].volume = vol;
        }

		public void PlayNarrationClip(ACTOR actor, int index, JOKE_ID jokeID)
		{
			// will add blink effect
			if(actor != ACTOR.DOG)
			{
				if(jokeID == JOKE_ID.HECKLE)
					AudioSources[(int)AUDIO_SOURCE_ID.VOICE_ACTING].clip = narrationClipDict[actor][index].Heckle;
				else if(jokeID == JOKE_ID.JOKE)
					AudioSources[(int)AUDIO_SOURCE_ID.VOICE_ACTING].clip = narrationClipDict[actor][index].Joke;
				else
					AudioSources[(int)AUDIO_SOURCE_ID.VOICE_ACTING].clip = narrationClipDict[actor][index].Setup;

				AudioSources[(int)AUDIO_SOURCE_ID.VOICE_ACTING].Play();
			}
		}

		public bool IsClipPlayingInSource(AUDIO_SOURCE_ID src)
		{
			return AudioSources[(int)src].isPlaying;
		}

		public void PlayRandomPositiveRiff()
		{
			int riffIndex = Random.Range(0, PositiveRiffClips.Count);
			AudioSources[(int)AUDIO_SOURCE_ID.RIFF_SOURCE].clip = PositiveRiffClips[riffIndex];
			AudioSources[(int)AUDIO_SOURCE_ID.RIFF_SOURCE].Play();
		}

		public void PlayRandomNegativeRiff()
		{
			int riffIndex = Random.Range(0, NegativeRiffClips.Count);
			AudioSources[(int)AUDIO_SOURCE_ID.RIFF_SOURCE].clip = NegativeRiffClips[riffIndex];
			AudioSources[(int)AUDIO_SOURCE_ID.RIFF_SOURCE].Play();
		}

		public void PlaySound(AUDIO_SOURCE_ID clipToPlay)
		{
			AudioSources[(int)clipToPlay].Play();
		}


        public void PlaySFXSound(SFX_ID sfxID)
        {
            AudioSources[(int)AUDIO_SOURCE_ID.SFX].clip = sfxClipDict[sfxID];
            AudioSources[(int)AUDIO_SOURCE_ID.SFX].Play();
        }
        public void PlayBoilingSFXSound(SFX_ID sfxID)
        {
            AudioSources[(int)AUDIO_SOURCE_ID.BOILING_SOURCE].clip = sfxClipDict[sfxID];
            AudioSources[(int)AUDIO_SOURCE_ID.BOILING_SOURCE].Play();
        }

        public void PlayCrowdSound(SFX_ID sfxID)
		{
			AudioSources[(int)AUDIO_SOURCE_ID.CROWD_SOURCE].clip = sfxClipDict[sfxID];
			AudioSources[(int)AUDIO_SOURCE_ID.CROWD_SOURCE].Play();
		}

		public void PlayMusicSound(MUSIC_ID musicID)
		{
			AudioSources[(int)AUDIO_SOURCE_ID.MUSIC_SOURCE].clip = musicClipDict[musicID];
			AudioSources[(int)AUDIO_SOURCE_ID.MUSIC_SOURCE].Play();
		}
	}
}