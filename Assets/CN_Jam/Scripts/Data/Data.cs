﻿using UnityEngine;
using System.Collections;

namespace Assets.Data
{
	public enum JOKE_ID
	{
		HECKLE,
		JOKE,
		SETUP
	};

	public enum STATE {
		INTRO,
		INITIALIZE_GAME,
		HECKLE,
		SETUP_READ,
		USER_SELECTING_OPTION,
		CORRECT_RESPONSE,
		LIMBO,
		MAX_STATES
	};

	public enum ACTOR
	{
		ASTRONAUT,
		FARMER,
		FISHERMAN,
		MAGICIAN,
		BALLERINA,
		POLICEMAN,
		GHOST,
		PUNK,
		DINOSAUR,
		DOG,
		JOE_CUPPA_HAPPY_DECAL,
        JOE_CUPPA_SAD_DECAL,
        JOE_CUPPA_BOILING_SPRITE,
        JOE_CUPPA,
        CORGI_BLINK,
		MAX_ACTOR
	}

	public class StringHashes {
		public static int CuppaPainHash = Animator.StringToHash("CuppaPain");
        public static int CuppaHappyHash = Animator.StringToHash("CuppaHappy");
        public static int CuppaBoilingHash = Animator.StringToHash("BoilingTrigger");
        public static int CorgiBlinkHash = Animator.StringToHash("CorgiBlink");
		public static int JoeCuppaMoveLeftToMid = Animator.StringToHash("JoeCuppaMoveLeftToMid");
		public static int JoeCuppaMoveRightToMid = Animator.StringToHash("JoeCuppaMoveRightToMid");
		public static int JoeCuppaMoveMidToLeft = Animator.StringToHash("JoeCuppaMoveMidToLeft");
		public static int JoeCuppaMoveMidToRight = Animator.StringToHash("JoeCuppaMoveMidToRight");
		public static int JoeCuppaMoveLeftToRight = Animator.StringToHash("JoeCuppaMoveLeftToRight");
		public static int JoeCuppaMoveRightToLeft = Animator.StringToHash("JoeCuppaMoveRightToLeft");
    }
}
